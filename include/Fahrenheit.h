#ifndef FAHRENHEIT_H
#define FAHRENHEIT_H
#include <Temperatura.h>

class Fahrenheit : public Temperatura
{
    public:
        Fahrenheit();
        void PrintFar();
    protected:

    private:
};

#endif // FAHRENHEIT_H
